#va chercher dans le hub l'image maven
FROM maven:3.6.3-jdk-13 as build1
WORKDIR /src
COPY pom.xml .
#copie pom dans src
#convertie les retours chariot linux en windows
RUN mvn dependency:go-offline
# copie les dependances
COPY src ./src
RUN mvn spring-javaformat:apply
RUN mvn package
EXPOSE 8080
#mettre un genre d'alias , metre un argument dans copie
FROM java
COPY --from=build1 src/target/*.jar .
CMD java -jar target/*.jar
